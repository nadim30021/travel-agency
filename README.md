# travel-agency
This is a mini travel-agency project.

# Technologies:
- Java 8
- Spring Boot
- Spring MVC
- Spring Security
- Spring Form
- JSP
- JSTL
- Hibernate
- Bootstarp
- Fontawsome
- Maven

# IDE:
- Intellij IDEA 2018.1.7

# DB:
- Mysql (prefarable)


All the DDLs are run during project run. And as all the initial db queries are writte in mysql so mysql db is preferable.

Nothing more complex thing to do. Just add maven run configuration in IntellIJ IDEA with:: Command Line -> spring-boot:run

Working branch: master (commit: 3d9cd5a4fbc61f10dafa0cb12518b90243257cdf)

# Initial Login User Password:
user: user, password 123

user: nadim, password: 123

Prerequisits: keep a schema in your mysql db named: travel_agency

