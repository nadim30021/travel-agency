alter table ta_post modify column posted_on datetime;
alter table ta_post modify column modified_on datetime;


INSERT INTO travel_agency.ta_config_type (id, deleted, description, inactive, name, value) VALUES ('locations', 0, 'Locations', 0, 'Locations', 'Locations');

INSERT INTO travel_agency.ta_app_config (deleted, description, inactive, name, ta_config_type_id) VALUES ( 0, 'Sylhet', 0, 'Sylhet', 'locations');
INSERT INTO travel_agency.ta_app_config (deleted, description, inactive, name, ta_config_type_id) VALUES ( 0, 'Bandarban', 0, 'Bandarban', 'locations');
INSERT INTO travel_agency.ta_app_config (deleted, description, inactive, name, ta_config_type_id) VALUES ( 0, 'Khulna', 0, 'Khulna', 'locations');

INSERT INTO travel_agency.ta_user_detail (address, deleted, email, father_name, first_name, gender, inactive, last_name, mother_name, phone, ta_user_id, DESIGNATION) VALUES ( 'Dhaka', 0, 'nadim@gmail.com', 'Anamul', 'Md. Nadimul Haque', 'M', 0, 'Bhuiyan', 'Nurjahan', '01827090222', 1, null);
INSERT INTO travel_agency.ta_user_detail (address, deleted, designation, email, father_name, first_name, gender, inactive, last_name, mother_name, phone, ta_user_id) VALUES ( null, 0, 'Admin', '', null, 'Root', null, 0, 'User', null, '', 2);

INSERT INTO travel_agency.ta_user (deleted, inactive, password, ta_config_type_id, username) VALUES ( 0, 0, '$2a$10$mls2vIDenxTAQluuslAu8OB6ij3XHc2SHqpjEPSzW4/WPduTk.nS2', null, 'nadim');
INSERT INTO travel_agency.ta_user (deleted, inactive, password, ta_config_type_id, username) VALUES ( 0, 0, '$2a$10$mls2vIDenxTAQluuslAu8OB6ij3XHc2SHqpjEPSzW4/WPduTk.nS2', null, 'user');

INSERT INTO travel_agency.ta_post (deleted, inactive, modified_on, posted_on, privacy, ta_app_config_id, status, ta_user_id) VALUES ( 0, 0, '2019-05-11 00:00:00', null, 'PUBLIC', 1, 'Test', 1);
INSERT INTO travel_agency.ta_post (deleted, inactive, modified_on, posted_on, privacy, ta_app_config_id, status, ta_user_id) VALUES ( 0, 0, '2019-05-11 00:00:00', null, 'PUBLIC', 2, 'This is my first public post. Add Another Line.', 1);
INSERT INTO travel_agency.ta_post (deleted, inactive, modified_on, posted_on, privacy, ta_app_config_id, status, ta_user_id) VALUES ( 0, 0, '2019-05-11 00:00:00', '2019-05-11 00:00:00', 'PUBLIC', null, 'This is my another public post.', 1);

INSERT INTO travel_agency.ta_post (deleted, inactive, modified_on, posted_on, privacy, ta_app_config_id, status, ta_user_id) VALUES ( 0, 0, '2019-05-11 00:00:00', null, 'PUBLIC', 3, 'Other than RedirectView , Spring provides another option to perform redirection,', 1);
INSERT INTO travel_agency.ta_post (deleted, inactive, modified_on, posted_on, privacy, ta_app_config_id, status, ta_user_id) VALUES ( 0, 0, '2019-05-11 00:00:00', null, 'PUBLIC', 1, 'This is another Post', 1);
