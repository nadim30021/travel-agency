package com.travelAgency.travelAgency.controllers;

import com.travelAgency.travelAgency.daos.TaAppConfigDao;
import com.travelAgency.travelAgency.models.TaPost;
import com.travelAgency.travelAgency.models.TaUser;
import com.travelAgency.travelAgency.roymvc.controller.RController;
import com.travelAgency.travelAgency.services.HomeService;
import com.travelAgency.travelAgency.services.TaPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping(value = "")
public class HomeController extends RController<HomeService> {

  @Autowired
  TaAppConfigDao taAppConfigDao;

  @RequestMapping(value = {"/", "/Home/"}, method = RequestMethod.GET)
  public Object home(HttpServletRequest request) {
    ModelAndView mv = (ModelAndView) this.service.prepareHomeData(request);
    List<TaPost> publicPostList = taPostService.getDao().findAllByPrivacyOrderByIdDesc("PUBLIC");
    mv.addObject("publicPostList", publicPostList);
    return mv;
  }

  @Autowired
  TaPostService taPostService;

  @RequestMapping(value = {"/", "/Home/"}, method = RequestMethod.POST)
  public Object post(HttpServletRequest request, @ModelAttribute("taPost") TaPost post) {
    boolean isSuccessful = taPostService.savePostData(post);
    return "redirect:/Home/";
  }

  @RequestMapping(value = "/Login/", method = RequestMethod.GET)
  public Object login(HttpServletRequest request) {
    ModelAndView mv = new ModelAndView();
    mv.addObject("taUser", new TaUser());

    mv.setViewName("Login");
    return mv;
  }

//  @RequestMapping(value = "/Login/", method = RequestMethod.POST)
//  public Object loginCheck(HttpServletRequest request, @ModelAttribute("taPost") TaUser user) {
//    boolean isSuccessful = this.service.checkLogin(user);
//    if (isSuccessful) return "redirect:/Home/";
//    return "redirect:/Login/";
//  }

  @RequestMapping(value = "/Posts/", method = RequestMethod.GET)
  public Object publicPosts(HttpServletRequest request) {
    ModelAndView mv = new ModelAndView();
    List<TaPost> publicPostList = taPostService.getDao().findAllByPrivacyOrderByIdDesc("PUBLIC");
    mv.addObject("publicPostList", publicPostList);

    mv.setViewName("Posts");
    return mv;
  }

  @RequestMapping(value="/Logout/", method = RequestMethod.GET)
  public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (auth != null){
      new SecurityContextLogoutHandler().logout(request, response, auth);
    }
    return "redirect:/Login/";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
  }

}
