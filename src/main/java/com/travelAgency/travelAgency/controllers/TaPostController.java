package com.travelAgency.travelAgency.controllers;

import com.travelAgency.travelAgency.daos.TaAppConfigDao;
import com.travelAgency.travelAgency.daos.TaUserDetailDao;
import com.travelAgency.travelAgency.models.TaPost;
import com.travelAgency.travelAgency.roymvc.controller.RController;
import com.travelAgency.travelAgency.services.TaPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/Post")
public class TaPostController extends RController<TaPostService> {

  @ResponseBody
  @RequestMapping(value = "/List/", method = RequestMethod.GET)
  public Object list(HttpServletRequest request) {
    return this.service.getDao().findAll();
  }


  @Autowired
  TaUserDetailDao taUserDetailDao;

  @Autowired
  TaAppConfigDao taAppConfigDao;

  @RequestMapping(value = "/Edit/{id}/", method = RequestMethod.GET)
  public Object edit(HttpServletRequest request, @PathVariable Integer id) {
    ModelAndView mv = new ModelAndView();
    Optional<TaPost> taPostOpt = this.service.getDao().findById(id);
    if (taPostOpt.isPresent()) {
      mv.addObject("taPost", taPostOpt.get());
    }
    else mv.addObject("taPost", new TaPost());

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String currentPrincipalName = authentication.getName();
    Optional<com.travelAgency.travelAgency.models.TaUserDetail> taUserDetailOpt = taUserDetailDao.findByUserUsername(currentPrincipalName);
    if (taUserDetailOpt.isPresent()) {
      com.travelAgency.travelAgency.models.TaUserDetail taUserDetail = taUserDetailOpt.get();

      if (taUserDetail != null) {
        mv.addObject("taUserDetail", taUserDetail);
      }

      List<com.travelAgency.travelAgency.models.TaAppConfig> locationList = taAppConfigDao.findByTaConfigTypeId("locations");
      if (locationList.size() > 0) mv.addObject("locationList", locationList);
    }
    else mv.addObject("taUserDetail", new com.travelAgency.travelAgency.models.TaUserDetail());

    mv.setViewName("post/Edit");
    return mv;
  }

  @RequestMapping(value = "/Edit/{id}/", method = RequestMethod.POST)
  public Object editSave(HttpServletRequest request, @PathVariable Integer id, @ModelAttribute("taPost") TaPost post) {
    boolean isSuccessful = this.service.updatePost(post);
    return "redirect:/Home/";
  }

  @RequestMapping(value = "/Delete/{id}/", method = RequestMethod.GET)
  public Object deleteSave(HttpServletRequest request, @PathVariable Integer id, @ModelAttribute("taPost") TaPost post) {
    try{
      this.service.getDao().delete(post);
      return "redirect:/Home/";
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return "redirect:/User/Profile/";
  }
}
