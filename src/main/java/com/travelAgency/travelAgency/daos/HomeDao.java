package com.travelAgency.travelAgency.daos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomeDao extends CrudRepository<com.travelAgency.travelAgency.models.TaAppConfig, Integer> {
}
