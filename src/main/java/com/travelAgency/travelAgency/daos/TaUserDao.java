package com.travelAgency.travelAgency.daos;

import com.travelAgency.travelAgency.models.TaUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaUserDao extends CrudRepository<TaUser, Integer> {
  Optional<TaUser> findByIdAndUsername(int i, String roy);

  Optional<TaUser> findByUsername(String userName);
}
