package com.travelAgency.travelAgency.daos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaAppConfigDao extends CrudRepository<com.travelAgency.travelAgency.models.TaAppConfig, Integer> {

  List<com.travelAgency.travelAgency.models.TaAppConfig> findByTaConfigTypeId(String configType);
}
