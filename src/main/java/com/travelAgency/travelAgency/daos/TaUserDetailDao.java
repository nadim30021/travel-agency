package com.travelAgency.travelAgency.daos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaUserDetailDao extends CrudRepository<com.travelAgency.travelAgency.models.TaUserDetail, Integer> {

  Optional<com.travelAgency.travelAgency.models.TaUserDetail> findByUserUsername(String roy);
}
