package com.travelAgency.travelAgency.daos;

import com.travelAgency.travelAgency.models.TaPost;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaPostDao extends CrudRepository<TaPost, Integer> {
  List<TaPost> findAllByPrivacyOrderByIdDesc(String privacy);

  List<TaPost> findAllByUserUsernameOrderByIdDesc(String username);
}
