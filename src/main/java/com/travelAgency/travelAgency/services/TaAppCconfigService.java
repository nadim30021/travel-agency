package com.travelAgency.travelAgency.services;

import com.travelAgency.travelAgency.daos.TaAppConfigDao;
import com.travelAgency.travelAgency.roymvc.service.RService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaAppCconfigService extends RService<TaAppConfigDao> {
  public List<com.travelAgency.travelAgency.models.TaAppConfig> listByConfigType(String configType) {
    List<com.travelAgency.travelAgency.models.TaAppConfig> list = this.dao.findByTaConfigTypeId(configType);
    return list;
  }
}
