package com.travelAgency.travelAgency.services;

import com.travelAgency.travelAgency.daos.TaUserDetailDao;
import com.travelAgency.travelAgency.roymvc.service.RService;
import org.springframework.stereotype.Service;

@Service
public class TaUserDetailService extends RService<TaUserDetailDao> {
}
