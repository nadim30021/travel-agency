package com.travelAgency.travelAgency.services;

import com.travelAgency.travelAgency.daos.HomeDao;
import com.travelAgency.travelAgency.daos.TaAppConfigDao;
import com.travelAgency.travelAgency.daos.TaUserDetailDao;
import com.travelAgency.travelAgency.models.TaPost;
import com.travelAgency.travelAgency.models.TaUser;
import com.travelAgency.travelAgency.roymvc.service.RService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
public class HomeService extends RService<HomeDao> {

  @Autowired
  TaUserDetailDao taUserDetailDao;

  @Autowired
  TaAppConfigDao taAppConfigDao;

  public Object prepareHomeData(HttpServletRequest request) {
    ModelAndView mv = new ModelAndView();
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String currentPrincipalName = authentication.getName();
    Optional<com.travelAgency.travelAgency.models.TaUserDetail> taUserDetailOpt = taUserDetailDao.findByUserUsername(currentPrincipalName);
    if (taUserDetailOpt.isPresent()) {
      com.travelAgency.travelAgency.models.TaUserDetail taUserDetail = taUserDetailOpt.get();

      if (taUserDetail != null) {
        mv.addObject("taUserDetail", taUserDetail);
      }

      List<com.travelAgency.travelAgency.models.TaAppConfig> locationList = taAppConfigDao.findByTaConfigTypeId("locations");
      if (locationList.size() > 0) mv.addObject("locationList", locationList);

      mv.addObject("taPost", new TaPost());

      mv.setViewName("Home");
    }
    else mv.addObject("taUserDetail", new com.travelAgency.travelAgency.models.TaUserDetail());
    return mv;
  }

  @Autowired
  TaUserService taUserService;

  public boolean checkLogin(TaUser user) {
    Optional<TaUser> userOpt = taUserService.getDao().findByUsername(user.getUsername());
    if (userOpt.isPresent()) {
      TaUser taUser = userOpt.get();
      if (taUser.getPassword() == null) return false;
      BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
      if (encoder.matches(user.getPassword(), taUser.getPassword())) return true;
    }
    return false;
  }
}
