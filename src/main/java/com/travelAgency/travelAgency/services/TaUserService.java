package com.travelAgency.travelAgency.services;

import com.travelAgency.travelAgency.daos.TaAppConfigDao;
import com.travelAgency.travelAgency.daos.TaUserDao;
import com.travelAgency.travelAgency.daos.TaUserDetailDao;
import com.travelAgency.travelAgency.models.TaPost;
import com.travelAgency.travelAgency.models.TaUser;
import com.travelAgency.travelAgency.roymvc.service.RService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class TaUserService extends RService<TaUserDao> {

  @Autowired
  TaUserDetailDao taUserDetailDao;

  @Autowired
  TaAppConfigDao taAppConfigDao;

  @Autowired
  TaPostService taPostService;

  public ModelAndView prepareProfileData() {
    ModelAndView mv = new ModelAndView();
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String currentPrincipalName = authentication.getName();
    Optional<com.travelAgency.travelAgency.models.TaUserDetail> taUserDetailOpt = taUserDetailDao.findByUserUsername(currentPrincipalName);
    if (taUserDetailOpt.isPresent()) {
      com.travelAgency.travelAgency.models.TaUserDetail taUserDetail = taUserDetailOpt.get();

      if (taUserDetail != null) {
        mv.addObject("taUserDetail", taUserDetail);
      }

      List<com.travelAgency.travelAgency.models.TaAppConfig> locationList = taAppConfigDao.findByTaConfigTypeId("locations");
      if (locationList.size() > 0) mv.addObject("locationList", locationList);

      mv.addObject("taPost", new TaPost());
    }
    else mv.addObject("taUserDetail", new com.travelAgency.travelAgency.models.TaUserDetail());

    List<TaPost> publicPostList = taPostService.getDao().findAllByUserUsernameOrderByIdDesc(currentPrincipalName);
    mv.addObject("publicPostList", publicPostList);

    mv.setViewName("user/Profile");
    return mv;
  }

  @Transactional
  public boolean saveUserDetail(com.travelAgency.travelAgency.models.TaUserDetail userDetail) {
    TaUser user = userDetail.getUser();
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    user.setPassword(encoder.encode(user.getPassword()));
    user.setInactive(0);
    user.setDeleted(0);
    try {
      TaUser newUser = this.dao.save(user);
      userDetail.setUserId(newUser.getId());
      userDetail.setInactive(0);
      userDetail.setDeleted(0);
      this.taUserDetailDao.save(userDetail);
      return true;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return false;
  }
}
