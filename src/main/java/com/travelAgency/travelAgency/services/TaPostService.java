package com.travelAgency.travelAgency.services;

import com.travelAgency.travelAgency.daos.TaPostDao;
import com.travelAgency.travelAgency.models.TaPost;
import com.travelAgency.travelAgency.roymvc.service.RService;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;

@Service
public class TaPostService extends RService<TaPostDao> {

  public boolean savePostData(TaPost post) {
    try {
      post.setPostedOn(LocalDate.now());
      post.setModifiedOn(LocalDate.now());
      this.dao.save(post);
      return true;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return false;
  }

  public boolean updatePost(TaPost post) {
    try {
      post.setModifiedOn(LocalDate.now());
      this.dao.save(post);
      return true;
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    return false;
  }
}
