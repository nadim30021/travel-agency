package com.travelAgency.travelAgency.services;

import com.travelAgency.travelAgency.daos.TaConfigTypeDao;
import com.travelAgency.travelAgency.roymvc.service.RService;
import org.springframework.stereotype.Service;

@Service
public class TaConfigTypeService extends RService<TaConfigTypeDao> {
  public Iterable list() {
    Iterable all = this.dao.findAll();
    return all;
  }
}
