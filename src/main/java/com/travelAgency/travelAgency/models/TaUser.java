package com.travelAgency.travelAgency.models;

import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;

@Entity
@Table(name = "TA_USER")
@EnableAutoConfiguration
@SQLDelete(sql = "UPDATE TA_USER SET DELETED = 1 WHERE id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted <> 1")
public class TaUser {

  public TaUser() {
  }

  private Integer id;

  private String username;

  private String password;

  private String taConfigTypeId;

  private Integer inactive;

  private Integer deleted;

  private TaConfigType type;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID", nullable = false)
  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "USERNAME", nullable = true, length = 50)
  public String getUsername() {
    return this.username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @Basic
  @Column(name = "PASSWORD", nullable = true, length = 100)
  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Basic
  @Column(name = "TA_CONFIG_TYPE_ID", nullable = true, length = 20)
  public String getTaConfigTypeId() {
    return taConfigTypeId;
  }

  public void setTaConfigTypeId(String taConfigTypeId) {
    this.taConfigTypeId = taConfigTypeId;
  }

  @Basic
  @Column(name = "INACTIVE", nullable = true)
  public Integer getInactive() {
    return inactive;
  }

  public void setInactive(Integer inactive) {
    this.inactive = inactive;
  }

  @Basic
  @Column(name = "DELETED", nullable = true)
  public Integer getDeleted() {
    return deleted;
  }

  public void setDeleted(Integer deleted) {
    this.deleted = deleted;
  }

  @ManyToOne
  @JoinColumn(name = "TA_CONFIG_TYPE_ID", insertable = false, updatable = false)
  public TaConfigType getType() {
    return this.type;
  }

  public void setType(TaConfigType type) {
    this.type = type;
  }
}
